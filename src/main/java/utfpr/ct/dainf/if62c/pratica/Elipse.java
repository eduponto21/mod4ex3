/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1906399
 */
public class Elipse implements FiguraComEixos {
	public double r, s;

	public Elipse(){
	}

	public Elipse(double r, double s){
		this.r = r;
		this.s = s;
	}

	@Override
	public double getArea(){
		double a = Math.PI*r*s;
		return a;
	}

	@Override
	public double getPerimetro(){
		double p = Math.PI * (3*(r+s) - Math.sqrt((3*r+s)*(r+3*s)));
		return p;
	}

	@Override
	public double getEixoMenor(){
		double e = 2*s;
		return e;
	}

	@Override
	public double getEixoMaior(){
		double e = 2*r;
		return e;
	}

	@Override
	public String getNome(){
		return "Elipse";
	}
    
}
