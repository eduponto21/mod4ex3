/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1906399
 */
public class Circulo extends Elipse {
	public double r;

	public Circulo(){}

	public Circulo(double raio){
		super(raio, raio);
		this.r = raio;
	}

	@Override
	public double getPerimetro(){
		double p = 2*Math.PI*this.r;
		return p;
	}
    	
	@Override
	public String getNome(){
		return "Circulo";
	}
}
