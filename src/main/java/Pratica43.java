import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author uckaah
 */
public class Pratica43 {
	public static void main(String[] args) {
		Circulo cir = new Circulo(4);
		Elipse el = new Elipse(3,4);
		Quadrado quad = new Quadrado(5);
		Retangulo ret = new Retangulo(4,9);
		TrianguloEquilatero tri = new TrianguloEquilatero(7);

		System.out.println("Area do circulo:" + cir.getArea());
       		System.out.println("Perimetro do circulo:" + cir.getPerimetro());

        	System.out.println("Area da elipse:" + el.getArea());
        	System.out.println("Perimetro da elipse:" + el.getPerimetro());

        	System.out.println("Area do quadrado:" + quad.getArea());
        	System.out.println("Perimetro do quadrado:" + quad.getPerimetro());

        	System.out.println("Area do retangulo:" + ret.getArea());
        	System.out.println("Perimetro do retangulo:" + ret.getPerimetro());

        	System.out.println("Area do triangulo:" + tri.getArea());
		System.out.println("Perimetro do triangulo:" + tri.getPerimetro());
	}
    
}
